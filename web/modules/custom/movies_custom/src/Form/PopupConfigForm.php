<?php

namespace Drupal\movies_custom\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;

/**
 * Class PopupConfigForm.
 *
 * Enable Popup Config Form for Homepage.
 */
class PopupConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'movies_custom.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_homepage_popup_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $current_user = \Drupal::currentUser();
    $config = $this->config('movies_custom.settings');

    $total = $form_state->get('total');

    if ($total === NULL) {
      $name_field = $form_state->set('total', 1);
      $total = 1;
    }
    $form['#tree'] = TRUE;
    $form['popup_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Homepage Popup Config Form'),
      '#prefix' => '<div id="names-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];
    if (!empty($config->get('count'))) {
      $total = $config->get('count');
    }
    $k = 1;
    for ($i = 0; $i < $total; $i++) {
      $form['popup_fieldset']['container'][$i] = [
        '#type' => 'details',
        '#title' => $this->t('Popup Detail (' . $k . ')'),
        '#open' => TRUE,
      ];
      $form['popup_fieldset']['container'][$i]['title'][$i] = [
        '#type' => 'textfield',
        '#title' => $this->t('Title'),
        '#default_value' => (isset($config->get('data')[$i]['title'][$i])) ? $config->get('data')[$i]['title'][$i] : NULL,
      ];
      $form['popup_fieldset']['container'][$i]['body'][$i] = [
        '#type' => 'textarea',
        '#title' => $this->t('Description'),
        '#default_value' => (isset($config->get('data')[$i]['body'][$i])) ? $config->get('data')[$i]['body'][$i] : NULL,
      ];
      $form['popup_fieldset']['container'][$i]['image'][$i] = [
        '#type' => 'managed_file',
        '#title' => $this->t('Image'),
        '#upload_location' => 'public://popup',
        '#upload_validators' => [
          'file_validate_extensions' => ['png gif jpg jpeg pdf doc'],
        ],
        '#default_value' => (isset($config->get('data')[$i]['image'][$i])) ? $config->get('data')[$i]['image'][$i] : NULL,
      ];
      $form['popup_fieldset']['container'][$i]['user'][$i] = [
        '#type' => 'hidden',
        '#default_value' => $current_user->id(),
      ];
      $k++;
    }

    $form['popup_fieldset']['actions'] = [
      '#type' => 'actions',
    ];
    $form['popup_fieldset']['actions']['add_popup'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add more'),
      '#submit' => ['::addMore'],
      '#ajax' => [
        'callback' => '::addMoreCallback',
        'wrapper' => 'names-fieldset-wrapper',
      ],
    ];

    if ($total > 1) {
      $form['popup_fieldset']['actions']['remove_name'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addMoreCallback',
          'wrapper' => 'names-fieldset-wrapper',
        ],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Callback for ajax buttons.
   */
  public function addMoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['popup_fieldset'];
  }

  /**
   * Submit handler for the 'add-more'.
   *
   * Increase total count.
   */
  public function addMore(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('total');
    $add_button = $name_field + 1;
    $form_state->set('total', $add_button);
    $count = count($form_state->getValue('popup_fieldset')['container']);
    $count = $count + 1;
    $this->config('movies_custom.settings')
      ->set('count', $count)
      ->save();

    $form_state->setRebuild();
  }

  /**
   * Submit handler for the 'remove one'.
   *
   * Decrease total count.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('total');
    if ($name_field > 1) {
      $remove_button = $name_field - 1;
      $form_state->set('total', $remove_button);
    }
    $count = count($form_state->getValue('popup_fieldset')['container']);
    $count = $count - 1;
    $this->config('movies_custom.settings')
      ->set('count', $count)
      ->save();

    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {
      // @todo Validate fields.
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    parent::submitForm($form, $form_state);
    $count = count($form_state->getValue('popup_fieldset')['container']);
    if (empty($count)) {
      $count = 1;
    }
    $container = $form_state->getValue('popup_fieldset')['container'];
    foreach ($container as $key => $value) {
      if (empty($value['title'][$key]) && empty($value['body'][$key]) && empty($value['image'][$key])) {
        $count = $count - 1;
      }
    }
    $image_data = $form_state->getValue('popup_fieldset')['container'];
    $image_array = [];
    foreach ($image_data as $key => $value) {
      if (reset($value['image'][$key])) {
        $file = File::load(reset($value['image'][$key]));
        if (!empty($file)) {
          $file_url = $file->getFileUri();
        }
        $style = ImageStyle::load('banner');
        $style_url = $style->buildUri($file_url);
        if (!file_exists($style_url)) {
          $style->createDerivative($file_url, $style_url);
        }
        $url = file_url_transform_relative($style->buildUrl($file_url));
        $image_array[] = $url;
      }
    }
    $this->config('movies_custom.settings')
      ->set('data', $form_state->getValue('popup_fieldset')['container'])
      ->set('count', $count)
      ->set('image', $image_array)
      ->save();
  }

}
