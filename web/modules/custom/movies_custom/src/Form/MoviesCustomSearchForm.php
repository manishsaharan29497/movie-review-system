<?php

namespace Drupal\movies_custom\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class MoviesCustomSearchForm.
 *
 * Custom Search form for Movies Page.
 */
class MoviesCustomSearchForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'movies_custom_search_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $movies = strip_tags(\Drupal::request()->query->get('movies'));
    $cast = strip_tags(\Drupal::request()->query->get('cast'));
    $form['movies'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Movie Name'),
      '#attributes' => [
        'placeholder' => [
          'Search By Movies',
        ],
        'class' => [
          'movies',
        ],
      ],
      '#default_value' => (isset($movies)) ? $movies : NULL,
      '#weight' => '0',
    ];
    $form['cast'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cast'),
      '#attributes' => [
        'placeholder' => [
          'Search By Cast',
        ],
        'class' => [
          'cast',
        ],
      ],
      '#default_value' => (isset($cast)) ? $cast : NULL,
      '#weight' => '0',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    $url = Url::fromRoute('movies_custom.movies')->toString();

    $form['reset'] = [
      '#type' => 'markup',
      '#markup' => '<a href="' . $url . '" class="btn btn-primary">Reset</a>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {
      // @todo Validate fields.
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    $movies = strip_tags($form_state->getValue('movies'));
    $cast = strip_tags($form_state->getValue('cast'));
    $url = Url::fromRoute('movies_custom.movies',
      ['movies' => $movies, 'cast' => $cast])->toString();
    $response = new RedirectResponse($url);
    $response->send();
  }

}
