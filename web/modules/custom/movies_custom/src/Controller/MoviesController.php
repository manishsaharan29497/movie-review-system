<?php

namespace Drupal\movies_custom\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;

/**
 * Class MoviesController.
 *
 * Movies Controller.
 */
class MoviesController extends ControllerBase {

  /**
   * Movies listing with searchable filter.
   */
  public function movies() {
    $build = [];

    // Filtering result.
    $movies = strip_tags(\Drupal::request()->query->get('movies'));
    $cast = strip_tags(\Drupal::request()->query->get('cast'));

    $qry = \Drupal::entityQuery('node');
    $qry->condition('type', 'movies');
    $qry->condition('status', 1);
    $movie_result = $qry->execute();

    $header = [
      ['data' => $this->t('Movies')],
      ['data' => $this->t('Cover Photo')],
      ['data' => $this->t('Release Date')],
    ];

    // All movies lists.
    $query = \Drupal::database()->select('node_field_data', 'node');
    $query->leftJoin('paragraphs_item_field_data', 'pifd', 'pifd.parent_id = node.nid');
    $query->leftJoin('node__field_movie_cast', 'cast', 'cast.entity_id = node.nid');
    $query->leftJoin('node__field_release_date', 'date', 'date.entity_id = node.nid');
    $query->leftJoin('users_field_data', 'users', 'users.uid = cast.field_movie_cast_target_id');
    $query->leftJoin('paragraph__field_photos', 'photos', 'photos.entity_id = pifd.id');
    $query->leftJoin('paragraph__field_cover_photo', 'cp', 'cp.entity_id = pifd.id');
    $query->fields('node', ['nid', 'title']);
    $query->fields('photos', ['field_photos_target_id']);
    $query->fields('date', ['field_release_date_value']);
    $query->condition('cp.field_cover_photo_value', '1', '=');
    // Implementing search queries.
    if (!empty($cast)) {
      $query->condition('users.name', '%' . $cast . '%', 'LIKE');
    }
    if (!empty($movies)) {
      $query->condition('node.title', '%' . $movies . '%', 'LIKE');
    }

    $result = $query->execute()->fetchAll();
    $temp = [];
    foreach ($result as $key => $value) {
      $temp[$value->nid] = [
        'nid' => $value->nid,
        'title' => $value->title,
        'image' => $value->field_photos_target_id,
        'date' => date('d M, Y', strtotime($value->field_release_date_value)),
      ];
    }

    $rows = [];
    foreach ($temp as $k => $val) {
      $file = File::load($val['image']);
      if (!empty($file)) {
        $file_url = $file->getFileUri();
      }

      $style = ImageStyle::load('medium');
      $style_url = $style->buildUri($file_url);
      if (!file_exists($style_url)) {
        $style->createDerivative($file_url, $style_url);
      }
      $url = file_url_transform_relative($style->buildUrl($file_url));

      $node_url = \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $val['nid']);

      $rows[] = [
        'data' => [
          'node' => $this->t('<a href="' . $node_url . '">' . $val['title'] . '</a>'),
          'image' => $this->t('<img src="' . $url . '">'),
          'date' => $val['date'],
        ],
      ];
    }
    $build['form'] = $this->formBuilder()->getForm('\Drupal\movies_custom\Form\MoviesCustomSearchForm');
    $build['config_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      "#empty" => $this->t("No record found."),
    ];
    $build['pager'] = [
      '#type' => 'pager',
    ];
    return $build;
  }

  /**
   * Add to watchlists.
   */
  public function addToWatchLists($id) {
    $current_user = \Drupal::currentUser();
    $query = \Drupal::entityQuery('node');
    $query->condition('type', 'watchlists');
    $query->condition('status', 1);
    $query->condition('field_user', $current_user->id(), '=');
    $query->condition('field_movies', $id, '=');
    $result = $query->execute();
    if (empty($result)) {
      $node = Node::create([
        'type'        => 'watchlists',
        'title'       => 'movie',
        'field_movies' => [
          'target_id' => $id,
        ],
        'field_user' => [
          'target_id' => $current_user->id(),
        ],
      ]);
      $node->save();
      \Drupal::messenger()->addMessage($this->t('Movie added to your watchlists.'));
    }
    else {
      \Drupal::messenger()->addMessage($this->t('Movie is already in your watchlists.'), 'warning');
    }
    $response = new RedirectResponse("/my-watchlists");
    $response->send();
  }

  /**
   * Remove from Watchlists.
   */
  public function removeFromWatchLists($id) {
    $current_user = \Drupal::currentUser();
    $query = \Drupal::entityQuery('node');
    $query->condition('type', 'watchlists');
    $query->condition('field_user', $current_user->id(), '=');
    $query->condition('field_movies', $id, '=');
    $result = $query->execute();
    if (!empty($result)) {
      $node = Node::load(reset($result));
      $node->delete();
    }
    \Drupal::messenger()->addMessage($this->t('Removed from Watchlists.'));
    $response = new RedirectResponse("/my-watchlists");
    $response->send();
  }

}
