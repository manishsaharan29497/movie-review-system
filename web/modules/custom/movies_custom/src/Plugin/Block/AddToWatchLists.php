<?php

namespace Drupal\movies_custom\Plugin\Block;

use Drupal\node\NodeInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;

/**
 * Provides a 'AddToWatchLists' block.
 *
 * @Block(
 *  id = "add_to_watchlists",
 *  admin_label = @Translation("Add to watchlists"),
 * )
 */
class AddToWatchLists extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $current_user = \Drupal::currentUser();
    $node = \Drupal::routeMatch()->getParameter('node');
    $query = \Drupal::entityQuery('node');
    $query->condition('type', 'watchlists');
    $query->condition('status', 1);
    $query->condition('field_user', $current_user->id(), '=');
    $query->condition('field_movies', $node->id(), '=');
    $result = $query->execute();
    if (empty($result)) {
      if ($node instanceof NodeInterface) {
        $url = Url::fromRoute('movies_custom.addtowatchlists', ['id' => $node->id()])->toString();
      }
      $build['#theme'] = 'add_to_watchlists';
      $build['add_to_watchlists']['#markup'] = '<a href="' . $url . '">Add to watchlists</a>';
    }
    else {
      if ($node instanceof NodeInterface) {
        $url = Url::fromRoute('movies_custom.removefromwatchlists', ['id' => $node->id()])->toString();
      }
      $build['#theme'] = 'remove_from_watchlists';
      $build['remove_from_watchlists']['#markup'] = '<a href="' . $url . '">Remove from watchlists</a>';
    }

    return $build;
  }

  /**
   * Prevent Block from cache.
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
