<?php

namespace Drupal\movies_custom\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\user\Entity\User;

/**
 * Provides a 'HomePagePopup' block.
 *
 * @Block(
 *  id = "homepage_popup",
 *  admin_label = @Translation("Homepage Popup Block"),
 * )
 */
class HomePagePopup extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Get config data from movie configuration form.
    $config = \Drupal::config('movies_custom.settings');
    $current_user = \Drupal::currentUser();
    $user_data = User::load($current_user->id());
    if (!empty($user_data)) {
      $fullname = $user_data->field_first_name->value . ' ' . $user_data->field_last_name->value;
    }
    $data = [];
    foreach ($config->get('data') as $key => $value) {
      $image_url = $config->get('image')[$key];
      if ($key == '0') {
        $active = 'active';
      }
      else {
        $active = '';
      }
      $data[] = [
        'title' => $value['title'][$key],
        'body' => $value['body'][$key],
        'image' => $image_url,
        'active' => $active,
        'name' => $fullname,
      ];
    }

    return [
      '#theme' => 'homepage_popup',
      '#data' => $data,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

}
